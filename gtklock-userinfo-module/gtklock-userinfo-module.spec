%global forgeurl https://github.com/jovanlanik/gtklock-userinfo-module
%global commit aa4b5832185d2dd9e7f88826fd9d03e3d5ea1ad6

Name:           gtklock-userinfo-module
Version:        4.0.0
%forgemeta
Release:        1%{?dist}
Summary:        Gtklock module adding user info to the lockscreen

License:        GPLv3
URL:            %{forgeurl}
Source0:        %{forgesource}

BuildRequires:  meson
BuildRequires:  gcc
BuildRequires:  pkgconfig(gtk+-3.0)
BuildRequires:  pkgconfig(accountsservice)

Requires:       gtklock%{?_isa} >= 4.0.0
Requires:       gtklock%{?_isa} < 5.0.0

Supplements:    gtklock%{?_isa}

%description
%{summary}


%prep
%forgeautosetup -p1

%build
%meson
%meson_build

%install
%meson_install


%files
%{_libdir}/gtklock/*.so
%license LICENSE
%doc README.md


%changelog
* Thu Nov 28 2024 Zephyr Lykos <fedora@mochaa.ws> - 4.0.0-1
- Update to 4.0.0 (snapshot aa4b5832185d2dd9e7f88826fd9d03e3d5ea1ad6)
- Lock major version

* Sun Apr 21 2024 Zephyr Lykos <fedora@mochaa.ws> - 3.0.0-2
- install plugins to libdir

* Fri Apr 19 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 3.0.0-1
- Update to 3.0.0

* Mon May 29 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 2.1.0-1
- Update to 2.1.0

* Sun Mar 19 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 2.0.0-2
- Fixed linter complains

* Sat Dec 31 2022 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 2.0.0-1
- Initial build
